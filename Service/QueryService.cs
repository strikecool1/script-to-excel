﻿internal class QueryService
{
    public static async Task<List<Students>> GetStudents()
    {
        var list = new List<Students>();
        using (FbConnection connection = new("database=192.168.250.72:Cont;user=sysdba;password=Vtlysq~Bcgjkby2020;Charset=win1251;"))
        {
            connection.Open();

            using var transaction = connection.BeginTransaction();
            string sql = " select first 1  s.famil||' '||s.name||' '||s.otch as fullname , fak.name ,  spec.name, s.foto " +
                            " from stud_gruppa sg " +
                            " inner join gruppa gr on gr.id = sg.grup_id " +
                            " inner join specialnost spec on specialnost.id = gr.spec_id " +
                            " inner join fakultet fak on fakultet.id = specialnost.fak_id " +
                            " inner join student s on student.id = sg.stud_id " +
                            " where s.id = 80346";

            using FbCommand command = new(sql, connection, transaction);
            FbDataReader reader = command.ExecuteReader();

            while (await reader.ReadAsync())
            {
                list.Add(new Students
                {
                    FullName = reader.GetString(0),
                    Faculty = reader.GetString(1),
                    Specialty = reader.GetString(2),
                    Url = (byte[])reader["foto"],

                });
            }
            reader.Close();
        }
        return list;

    }

    public static async Task<List<Workers>> GetWorkers()
    {
        List<Workers> list = new();

        using (FbConnection connection = new("database=192.168.250.72:Pers;user=sysdba;password=Vtlysq~Bcgjkby2020;Charset=win1251;"))
        {
            connection.Open();
            using var transaction = connection.BeginTransaction();
            using FbCommand command = new(" select  oname , dname , fio , img from KEKOS(0) where img is not null", connection, transaction);
            FbDataReader reader = command.ExecuteReader();

            while (await reader.ReadAsync())
            {
                list.Add(new Workers
                {
                    Department = reader.GetString(0),
                    Position = reader.GetString(1),
                    FullName = reader.GetString(2),
                    Url = (byte[])reader["img"],

                });
            }
            reader.Close();
        }
        return list;
    }
}

