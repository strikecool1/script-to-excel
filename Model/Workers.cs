
public class Workers
{
    public string FullName { get; set; } = string.Empty;
    public string Department { get; set; } = string.Empty;
    public string Position { get; set; } = string.Empty;
    public string NameUrl { get; set; } = string.Empty;
    public byte[] Url { get; set; }
}