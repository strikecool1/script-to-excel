
public class Students
{
    public string FullName { get; set; } = string.Empty;
    public string Faculty { get; set; } = string.Empty;
    public string Specialty { get; set; } = string.Empty;
    public string NameUrl { get; set; } = string.Empty;
    public byte[] Url { get; set; }

}