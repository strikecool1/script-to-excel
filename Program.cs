﻿// Регистрация кодировки win 1251 (Потому что в Net. 5 её не существует, нужно регистровать провайдер вручную )
Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
Encoding.GetEncoding(1252);


Excel.Application? application = null;
Excel.Workbooks? workbooks = null;
Excel.Workbook? workbook = null;
Excel.Sheets? worksheets = null;
Excel.Worksheet? worksheet = null;
//переменная для хранения диапазона ячеек
//в нашем случае - это будет одна ячейка
Excel.Range? cell = null;
try
{
    Console.WriteLine("Выберите на кого нужно генерировать excel...");
    Console.WriteLine("1 : Сотрдуники");
    Console.WriteLine("2 : Студенты");

    string? ValueKey = Console.ReadLine();
    string Path = "Image";

    if (!string.IsNullOrEmpty(ValueKey))
    {
        if (ValueKey == "1")
        {
            application = new Excel.Application
            {
                Visible = true
            };
            workbooks = application.Workbooks;
            workbook = workbooks.Add();
            worksheets = workbook.Worksheets; //получаем доступ к коллекции рабочих листов
            worksheet = worksheets.Item[1];//получаем доступ к первому листу


            //cell = worksheet.Cells[1, 1];//получаем доступ к ячейке
            //cell.Value = "Hello Excel";//пишем строку в ячейку A1

            // Сотрдуники
            IList<Workers> images = await QueryService.GetWorkers();
            // Создаём каталог для хранения jpg
            DirectoryInfo dirInfo = new(Path);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            for (int i = 0; i < images.Count; i++)
            {
                string[] parts = images[i].FullName.Split(' ');
                string latinName = Converting.ConvertToLatin(parts[0]).ToLower();
                string fullPath = $"{Path}//{i + 1}{latinName}.jpg";

                cell = worksheet.Cells[i + 1, 1];
                cell.Value = images[i].FullName; // ФИО

                cell = worksheet.Cells[i + 1, 2];
                cell.Value = images[i].Position; // Должность

                cell = worksheet.Cells[i + 1, 3];
                cell.Value = images[i].Department; // Отдел

                cell = worksheet.Cells[i + 1, 4];
                cell.Value = $"{i + 1}{latinName}.jpg"; // Фото

                using FileStream imageFile = new(fullPath, FileMode.Create);
                imageFile.Write(images[i].Url, 0, images[i].Url.Length);
                imageFile.Flush();
                //Excel.Range oRange = (Excel.Range)worksheet.Cells[i + 1, 1];
            }

            application.Quit();
        }
        else
        {
            // Студенты
            IList<Students> stud = await QueryService.GetStudents();

            for (int i = 0; i < stud.Count; i++)
            {
                string[] parts = stud[i].FullName.Split(' ');
                string latinName = Converting.ConvertToLatin(parts[0]).ToLower();
                string fullPath = $"{Path}//{i + 1}{latinName}.jpg";

                cell = worksheet.Cells[i + 1, 1];
                cell.Value = stud[i].FullName; // ФИО

                cell = worksheet.Cells[i + 1, 2];
                cell.Value = stud[i].Faculty; // Факультет

                cell = worksheet.Cells[i + 1, 3];
                cell.Value = stud[i].Specialty; // Специальность

                cell = worksheet.Cells[i + 1, 4];
                cell.Value = $"{i + 1}{latinName}.jpg"; // Фото

                using FileStream imageFile = new(fullPath, FileMode.Create);
                imageFile.Write(stud[i].Url, 0, stud[i].Url.Length);
                imageFile.Flush();

            }


        }
    }
   
}
finally
{
    //освобождаем память, занятую объектами
    Marshal.ReleaseComObject(o: workbook);
    Marshal.ReleaseComObject(o: workbooks);
    Marshal.ReleaseComObject(o: application);
}